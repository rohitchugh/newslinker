from gensim import corpora, models, similarities
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

#Get The Dictionary Which We Created Earlier
dictionary = corpora.Dictionary.load('Generated/btp.dict')

#Load the Corpus we Stored in Market Matrix Format Earlier
corpus = corpora.MmCorpus('Generated/btp.mm')

#Apply LDA and Extract Topics out of it.
lda = models.ldamodel.LdaModel(corpus=corpus, id2word=dictionary, num_topics=200, passes=10)
lda.save('Generated/model.lda')
#array = lda.show_topics(200)
