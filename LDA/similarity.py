from gensim import corpora, models, similarities
from serialize import documents
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

#Get The Dictionary Which We Created Earlier
dictionary = corpora.Dictionary.load('Generated/btp.dict')

#Load the Corpus we Stored in Market Matrix Format Earlier
corpus = corpora.MmCorpus('Generated/btp.mm')

#Get Model Which We Created Earlier
lda = models.LdaModel.load('Generated/model.lda')

doc = "technology"
vec_bow = dictionary.doc2bow(doc.lower().split())
vec_lda = lda[vec_bow] #Convert the query to LDA Space

index = similarities.MatrixSimilarity.load('Generated/bbc_business.index')
sims = index[vec_lda]


for i in sims:
    if i > 0.2:
        print i

exit(0)
#print(list(enumerate(sims)))
sims = sorted(enumerate(sims), key=lambda item: -item[1])
#print(sims) #Print Sorted (document number, similarity score)
for i in sims:
    if i[1] > 0.2:
        print i[0], " == ", i[1]
        print corpus[i[0]]
        print documents[i[0]]
