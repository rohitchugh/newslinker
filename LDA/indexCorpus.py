from gensim import corpora, models, similarities
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

#Get The Dictionary Which We Created Earlier
dictionary = corpora.Dictionary.load('Generated/btp.dict')

#Load the Corpus we Stored in Market Matrix Format Earlier
corpus = corpora.MmCorpus('Generated/btp.mm')

#Get Model Which We Created Earlier
lda = models.LdaModel.load('Generated/model.lda')

corpus_lda = lda[corpus] #Transform Corpus to LDA space and index it

'''
for topic in lda.show_topics(100):
    print topic
print type(corpus_lda)
for doc in corpus_lda[:2]:
    print doc
'''

index = similarities.MatrixSimilarity(corpus_lda) #Index Corpus( i.e. in LDA Space)
index.save('Generated/bbc_business.index')
