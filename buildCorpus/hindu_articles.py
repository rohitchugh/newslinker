'''
    Python 3
'''
from bs4 import BeautifulSoup
from multiprocessing import Pool
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
import pandas as pd
import requests
import pickle


class article:
    article_list = []

    def __init__(self, **kargs):
        self.text = kargs['text']
        self.tokens = kargs['tokens']
        self.date = kargs['pub_date']
        self.id = kargs['id']
    
    @staticmethod
    def save(date):
        '''
            Pickle Article Links
        '''
        with open('savedCorpus/test_oops.pickle', 'wb') as fp:
                pickle.dump(article.article_list, fp)

class url:
    stop_words = stopwords.words("english")

    def __init__(self, date, url_list):
        '''
            Build URL Index
        '''
        self.url_list = url_list
        self.date = date
        self.it = 1
    
    def getArticleData(self):
        '''
            Get Article Data
        '''
        how_many = 25
        p = Pool(processes=how_many)
        self.data = p.map(self.__parseArticlePage, [ url for url in self.url_list ])
        p.close()

    def __parseArticlePage(self, url):
        '''
            Parse Article Page
        '''
        try:
            resp = requests.get(url)
            soup = BeautifulSoup(resp.content, 'html.parser')
            text = ''
            text = [''.join((node.find_all(text=True))) for node in soup.find_all("p") ]
            text = ' '.join(text)
            tokens = word_tokenize(result)
            tokens = [ word.lower() for word in tokens if word.isalpha() and word.lower() not in url.stop_words ]
            
            result = dict()
            result['text'] = text
            result['tokens'] = tokens
            result['pub_date'] = date
            result['id'] = date + '_' + self.it
            new_article = article(result)
            article.article_list.append(new_article)

            self.it += 1

        except Exception as e:
            print(str(e))
            print("Error in url: ", url)


if __name__ == '__main__':
    pkl_file = open("savedCorpus/hindu_corpus", "rb")
    url_index = pickle.load(pkl_file)
    pkl_file.close()

    keylist = list(url_index.keys())
#    for key in url_index.keys()[:1]:
    for key in keylist:
        url_obj = url(key, url_index[key])
        url_obj.getArticleData()
        article.save('key')
        del url_obj


'''
def getUrls():
    pkl_file = open("savedCorpus/hindu_corpus", "rb")
    url_index = pickle.load(pkl_file)
#    url_index = [ url for row in url_index for url in row ]
    pkl_file.close()
    return url_index

def getArticleData(url_index):
    how_many = 25
    test_index = url_index[:50000]
    p = Pool(processes=how_many)
    data = p.map(parseArticlePage, [ url for url in test_index ])
    p.close()
    return data

def parseArticlePage(url):
    try:
        global stop_words
        resp = requests.get(url)
        soup = BeautifulSoup(resp.content, 'html.parser')
        result = dict()
        text = ''
        text = [''.join((node.find_all(text=True))) for node in soup.find_all("p") ]
        text = ' '.join(text)
        result['text'] = text
        tokens = word_tokenize(result)
        tokens = [ word.lower() for word in result if word.isalpha() and word.lower() not in stop_words ]
        result['tokens'] = tokens
        return result
    except Exception as e:
        print(str(e))
        print("Error in url: ", url)
        return []

def pickleData(data):
    with open('savedCorpus/hindu_article_corpus.pickle', 'wb') as fp:
            pickle.dump(data, fp)

def main():
    url_index = getUrls()
    article_data = getArticleData(url_index)
    pickleData(article_data)
    print("COMPLETED")

'''
