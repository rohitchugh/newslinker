'''
    Python3
'''
from gensim import corpora
from collections import defaultdict
import logging
import pickle
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

pkl_file = open("savedCorpus/hindu_article_corpus.pickle", "rb")
article_db = pickle.load(pkl_file)

token_all = [ token for document in article_db for token in document ]

# Find frequency of each token in token_all
frequency = defaultdict(int)
for token in token_all:
    frequency[token] += 1

# Removed all tokens which occurs once one in token_all
token_all = [ [token for token in document if frequency[token] > 1] for document in article_db ]

dictionary = corpora.Dictionary(token_all)
dictionary.save('savedCorpus/tokens.dict')

corpus = [ dictionary.doc2bow(document) for document in article_db ]
corpora.MmCorpus.serialize('savedCorpus/article_corpus.mm', corpus)
for document in article_db[:10]:
    print(document)
