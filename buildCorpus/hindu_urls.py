'''
    Python 3
'''
from bs4 import BeautifulSoup
from multiprocessing import Pool
import pandas as pd
import datetime
import requests
import pickle

def buildSources(start, end):
    '''
        Build URL Sources
    '''
    index = pd.date_range(start,end)
    base = 'http://www.thehindu.com/archive/print'
    url_index = [ base+'/'+str(i.year)+'/'+str(i.month).zfill(2)+'/'+str(i.day).zfill(2) for i in index ]
    return url_index

def getLinks(url_index):
    '''
        Get Article Links from Sources
    '''
    how_many = 50
    p = Pool(processes=how_many)
    data = p.map(parsePage, [ url for url in url_index ])
    p.close()
    data_dict = {k: v for d in data for k, v in d.items()}
    return data_dict

def parsePage(url):
    '''
        Parse Sources
    '''
    try:
        pub_date_raw = url.split('/')
        pub_date = pub_date_raw[5]+'-'+pub_date_raw[6]+'-'+pub_date_raw[7]

        resp = requests.get(url)
        soup = BeautifulSoup(resp.content, 'html.parser')
        links = soup.find_all("ul", {"class": "archive-list"})

        result = dict()
        result[pub_date] = [ li.get("href") for link in links for li in link.find_all("a") ]

        print(len(result[pub_date]))
        return result
    except Exception as e:
        print(str(e))
        print("Error in url: " , url)
        return []


def pickleData(data):
    '''
        Pickle Article Links
    '''
    with open('savedCorpus/hindu_corpus', 'wb') as fp:
            pickle.dump(data, fp)

def main():
    start = datetime.date(2006,1,1)
    end = datetime.date(2017,12,31)

    sources = buildSources(start, end)
    links = getLinks(sources)
    pickleData(links)

if __name__ == '__main__':
    main()
    print("COMPLETED")

    '''    
    resp = requests.get('http://www.thehindu.com/thehindu/2012/12/23/')
    print("GOT RESP")
    soup = BeautifulSoup(resp.content, 'lxml')
    result = soup.find_all("ul", {"class": "archive-list"})
    print(result[0].find_all("a")[0].get("href"))
    '''
