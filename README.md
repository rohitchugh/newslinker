NewsLinker
==========

Thematic Linking of News Articles and their Representation in Timelined Format

1.Serialize the documents and strore them in Market Matrix Format.  
$$ python serialize.py  
  
2.Generate the model and train it from our previously saved corpus.  
$$ python train.py  
  
3.Transform the corpus into LDA space and index it.  
$$ python indexCorpus.py  
  
4.Add the query in variable doc (line: 15) and get documents similar to query.  
$$ python similarity.py
---
## lda.py
**documents** = [ article1, article2, article3, ..... , article510]  
**texts** = list of words from each document - stopwords  
**all\_tokens** = list of all words from all documents  
**tokens\_ nce** = words occuring only once  
**texts** = texts - tokens _ once  
**dictionary** = word:ID ( btp.dict )  
**corpus** = list of documents where each document is present in terms of list of tuples of (word, frequency) in that document ( btp.mm )  
---
## lda2.py  
**dictionary** = [ (word:id), ( ... ), ...... , ( ... ) ]  
**corpus** = [ [ (w,id), (w,id), ... , (w,id) ], [ (w,id), (w,id), ... , (w,id) ], . . . . . , [ (w,id), (w,id), ... , (w,id) ] ]  
**lda** = modes.lda...( )  
>train model  
>apply lda and extract topic  
>lda.show\_topics() gives topics  


**corpus\_lda** = lda[corpus]  


>execute model  
>transform corpus to LDA space and index it  
>[ [ (1, 0.802), (27, 0.32), ... ], . . . , [ (), (), ...., ()] ]  
>list of documents --> containing list of topics --> defining topic relevance  


**doc** = " " --> query  
**vec\_bow** = make "corpus" (vector) of query  
**vec\_lda** = transform into LDA space (execute in LDA model)


